<?php

use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('/start', BotManController::class . '@startConversation');

$botman->hears('Betika', BotManController::class . '@startConversation');

$botman->hears('SportPesa', BotManController::class . '@startConversation');

$botman->hears('Tips', BotManController::class . '@startConversation');

$botman->hears('Subscribe', BotManController::class . '@startConversation');

$botman->hears('My Subscription', BotManController::class . '@startSubscription');

//-1001090135233
$botman->group(['recipient' => ['-1001090135233']], function ($bot) {
    $bot->hears('keyword', function ($bot) {

        $bot->reply("Hi");
        // Only listens when recipient '1234567890', '2345678901' or '3456789012' is receiving the message.
    });
});
$botman->fallback(function ($bot) {
//    $bot->say('Hehe', '-1001090135233',\BotMan\Drivers\Telegram\TelegramDriver::class);
    $bot->reply('Sorry, I did not understand these commands. Here is a list of commands I understand: ...');

});


$botman->hears('mush', function (\BotMan\BotMan\BotMan $botMan) {
    $botMan->reply($botMan->getUser());
});

$botman->on('group_chat_created', function ($payload, $bot) {
    $bot->reply("Kennedy");
});